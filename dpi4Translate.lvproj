﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="dpi4LavLIB_Useful" Type="Folder">
			<Item Name="Convert Panel to Screen Coordinate.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Convert Panel to Screen Coordinate.vi"/>
			<Item Name="Create folder if not exist.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Create folder if not exist.vi"/>
			<Item Name="CSV load.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/CSV load.vi"/>
			<Item Name="Excel export ArrayOfstr.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Excel export ArrayOfstr.vi"/>
			<Item Name="Excel open.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Excel open.vi"/>
			<Item Name="File Dialog.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/File Dialog.vi"/>
			<Item Name="Get All Controls Ref From VI.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Get All Controls Ref From VI.vi"/>
			<Item Name="Get All inserted Controls Ref From Control.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Get All inserted Controls Ref From Control.vi"/>
			<Item Name="Get EXE name.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Get EXE name.vi"/>
			<Item Name="Get Exe Ver.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Get Exe Ver.vi"/>
			<Item Name="Get monitor resolution.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Get monitor resolution.vi"/>
			<Item Name="INI_SaveLoad.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/INI_SaveLoad.vi"/>
			<Item Name="IS_EXE.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/IS_EXE.vi"/>
			<Item Name="Make Top VI Frontmost.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Make Top VI Frontmost.vi"/>
			<Item Name="Make VI Frontmost.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Make VI Frontmost.vi"/>
			<Item Name="MCL or Tbl 2 Excel.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/MCL or Tbl 2 Excel.vi"/>
			<Item Name="Postfix2expV2_point.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Postfix2expV2_point.vi"/>
			<Item Name="Postfix_ENG_2_RUS.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Postfix_ENG_2_RUS.vi"/>
			<Item Name="RemoveBadSymbols (SubVI).vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/RemoveBadSymbols (SubVI).vi"/>
			<Item Name="RunDifferentFile.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/RunDifferentFile.vi"/>
			<Item Name="RunVI.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/RunVI.vi"/>
			<Item Name="SaveFileAsSTR.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/SaveFileAsSTR.vi"/>
			<Item Name="Set VI screen coordinates.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Set VI screen coordinates.vi"/>
			<Item Name="Set Window Z-Position (hwnd).vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Set Window Z-Position (hwnd).vi"/>
			<Item Name="Startup Add EXE to it.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Startup Add EXE to it.vi"/>
			<Item Name="Startup Delete EXE from it.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Startup Delete EXE from it.vi"/>
			<Item Name="Startup Is EXE in it.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Startup Is EXE in it.vi"/>
			<Item Name="StopOrExit (SubVI).vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/StopOrExit (SubVI).vi"/>
			<Item Name="STR_translit.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/STR_translit.vi"/>
			<Item Name="dpi4_Format_Value.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/dpi4_Format_Value.vi"/>
			<Item Name="Variable_SaveLoad.vi" Type="VI" URL="../../dpi4_lavlib_useful/!Builds/dpi4LavLIB_Useful.llb/Variable_SaveLoad.vi"/>
		</Item>
		<Item Name="Privat" Type="Folder" URL="../SCR/Privat">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Public" Type="Folder" URL="../SCR/Public">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="1 UTrans_Example.vi" Type="VI" URL="../1 UTrans_Example.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Write Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value Simple.vi"/>
				<Item Name="Write Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value Simple STR.vi"/>
				<Item Name="Write Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value.vi"/>
				<Item Name="Write Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value STR.vi"/>
				<Item Name="Write Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value DWORD.vi"/>
				<Item Name="Write Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value Simple U32.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Delete Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Delete Registry Value.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
			</Item>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="dpi4Translate" Type="Source Distribution">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{B6AE0EF8-BF4A-4DFA-AC54-20A628EA7A8F}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">dpi4Translate</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">vi.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[1]" Type="Path">resource/objmgr</Property>
				<Property Name="Bld_excludedDirectory[1].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[2]" Type="Path">/C/ProgramData/National Instruments/InstCache/15.0</Property>
				<Property Name="Bld_excludedDirectory[3]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[3].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[4]" Type="Path">user.lib</Property>
				<Property Name="Bld_excludedDirectory[4].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">5</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME.llb</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{6C8848E8-A6DF-4F93-892F-8DEA6F668934}</Property>
				<Property Name="Bld_version.build" Type="Int">18</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME.llb</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].type" Type="Str">LLB</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{9FB163DC-7CAC-4A47-B38F-B94AECE27429}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Privat</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Public</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[3].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/dpi4LavLIB_Useful</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Exclude</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/1 UTrans_Example.vi</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">5</Property>
			</Item>
			<Item Name="Example" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{1FE3F914-BBAE-4F01-AC39-3A6F98A46222}</Property>
				<Property Name="App_INI_GUID" Type="Str">{BCAC1031-9B81-4A4C-85AC-42F0B603CEDF}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{C8E774E0-9EBE-4517-84D7-BCC4E6FAC173}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Example</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{99FA49DE-7A04-49E9-AF91-16F714BE02B8}</Property>
				<Property Name="Bld_version.build" Type="Int">6</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Example.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/Example.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{9FB163DC-7CAC-4A47-B38F-B94AECE27429}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/1 UTrans_Example.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">dpi4</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Example</Property>
				<Property Name="TgtF_internalName" Type="Str">Example</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2018 MaxCo</Property>
				<Property Name="TgtF_productName" Type="Str">Example</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{F3E33B06-C400-46CE-8B04-39E85C09CFBE}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Example.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
